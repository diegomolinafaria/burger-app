import React from 'react'
import classes from './Button.module.css';

const Button = (props) => (
    <button //Join serve para juntar o array de strings dentro da className
        className={[classes.Button, classes[props.btnType]].join(' ')}
        onClick={props.clicked}
        >{props.children}</button>
);

export default Button;