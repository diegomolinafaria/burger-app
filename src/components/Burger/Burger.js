import React from 'react';

import classes from './Burger.module.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';
const Burger = (props) => {

    // As linhas comentadas são a explicação da função abaixo.

    //props.test -> É um Objeto.
    // const objeto = props.test;
    //console.log(objeto);

    //Transformou o Objeto props.test em um Array
    // const array = Object.keys(props.test);
    // console.log(array);

    //Percorreu o Array com a função MAP, 
    // const array = Object.keys(props.test).map(key => {
    //     //Inseriu uma key(contador) para caso tenha o mesmom valor mais de uma vez
    //     //Inseriu um type para saber qual é o valor correto que precisa
    //     return[...Array(props.test[key])].map((_,i) => {
    //         return <BurgerIngredient key={key +i} type={key} />;
    //     })
    // });
    // console.log(array);

    //Explicação dessa função comentada acima
    let transformedIngredients = Object.keys(props.ingredients)
    .map(igKey => {
        return[...Array(props.ingredients[igKey])].map((_, i) => {
            return <BurgerIngredient key={igKey + i} type={igKey} />;
        })
    })
    //Reduce transforma o array em outro valor, dessa maneira só aparece os ingredients que tem uma quantidade diferente de 0 ou null.
    .reduce((arr, el) => {
        return arr.concat(el);
    }, []);

    if (transformedIngredients.length === 0) {
        transformedIngredients = <p>Please start adding ingredients</p>
    }

    return(
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top" />
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
};

export default Burger;