import React from 'react';
import classes from './Logo.module.css'
import burgerlogo from '../../assets/images/original.png'

const Logo = (props) => (
    <div className={classes.Logo}>
        <img src={burgerlogo} alt="MyBurger"/>
    </div>
);

export default Logo
