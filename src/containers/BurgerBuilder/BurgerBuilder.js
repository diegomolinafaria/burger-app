import React, {Component} from 'react';

import Aux from '../../hoc/Auxiliary/auxiliary';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';


//Usar tudo em maiúscula quando for GLOBAL
const INGREDIENT_PRICES = {
    salad:0.5,
    cheese: 0.4,
    bacon: 1,
    meat: 1.3,
}

class BurgerBuilder extends Component
{
    //Pode usar o STATE assim, mas agora existe uma versão mais moderna
    // constructor(props) {
    //     super(props);
    //     this.state = {...}
    // }

    // Modern state way \/
    state = {
        ingredients: {
            salad: 0,
            bacon: 0,
            cheese: 0,
            meat: 0
        },
        totalPrice: 4,
        purchasable: false,
        purchasing: false,
     
    //Para o STATE funcionar, é necessário ser passado no Component via props
    // test: {
    //     diego: 1,
    //     isabela: 2
    // }
    }

    updatePurchaseState (ingredients) {
        const sum = Object.keys(ingredients)
        .map(igKey => {
            return ingredients[igKey]
        })
        .reduce((sum, el) => {
            return sum + el;
        }, 0);
        this.setState({purchasable: sum > 0});
    }

    purchaseHandler = () => {
        this.setState({purchasing: true});
    }

    addIngredientHandler = (type) => {
        //Update the ingredients
        const oldCount = this.state.ingredients[type];
        const updatedCount = oldCount + 1;
        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = updatedCount;
        //Update the price
        const priceAddition = INGREDIENT_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAddition;

        this.setState({totalPrice: newPrice, ingredients: updatedIngredients, qtd: updatedIngredients});
        this.updatePurchaseState(updatedIngredients);
    }
    removeIngredientHandler = (type) => {
       //Update the ingredients
       const oldCount = this.state.ingredients[type];
       if (oldCount <= 0) {
           return; //Usado para não ter ingredients negativos
       }
       const updatedCount = oldCount - 1;
       const updatedIngredients = {
           ...this.state.ingredients
       };
       updatedIngredients[type] = updatedCount;
       //Update the price
       const priceDeduction = INGREDIENT_PRICES[type];
       const oldPrice = this.state.totalPrice;
       const newPrice = oldPrice - priceDeduction;

       this.setState({
           totalPrice: newPrice,
           ingredients: updatedIngredients,
           qtd: updatedIngredients
        });
        this.updatePurchaseState(updatedIngredients)
    }

    purchaseCancelHandler = () => {
        this.setState({purchasing: false});
    }

    purchaseContinueHandler = () => [
        alert('Coma seu burgão')
    ]

    render(){
        //Lógica para desativar o botão LESS se não houver ingredientes
        const disabledInfo = {
            ...this.state.ingredients
        };
        for(let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
            //Resultado se for igual ou menor que ZERO retorna TRUE e se for TRUE desativa o botão
        }

        return (
            <Aux>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                    <OrderSummary
                        ingredients={this.state.ingredients}
                        modalClosed={this.purchaseCancelHandler}
                        modalContinue={this.purchaseContinueHandler}
                        price={this.state.totalPrice} />
                </Modal>
                {/* Passado via PROPS o STATE test para poder ver os atributos no Component Burger.js */}
                {/* <Burger ingredients={this.state.ingredients} test={this.state.test} /> */}
                <Burger ingredients={this.state.ingredients} />
                <BuildControls
                ingredientAdded={this.addIngredientHandler}
                ingredientRemoved={this.removeIngredientHandler}
                disabled={disabledInfo}
                price={this.state.totalPrice}
                qtd={this.state.ingredients}
                purchasable={this.state.purchasable}
                purchasing={this.purchaseHandler} />
            </Aux>
        );
    }

}

export default BurgerBuilder;